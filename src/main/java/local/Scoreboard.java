package local;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.ArrayList;
import java.util.List;

public class Scoreboard {
    private final Map<OpponentsPair, ScoresPair> scores = new HashMap<>();
    private final List<OpponentsPair> opponentsSortedByStart = new ArrayList<>();

    public void upsertMatch(OpponentsPair opponents) {
        upsertMatch(opponents, new ScoresPair(0, 0));
    }

    public void upsertMatch(OpponentsPair opponents, ScoresPair scores) {
        this.scores.put(opponents, scores);
        if (!opponentsSortedByStart.contains(opponents))
            opponentsSortedByStart.add(opponents);
    }

    public void removeMatch(OpponentsPair opponentsPair) {
        scores.remove(opponentsPair);
        opponentsSortedByStart.remove(opponentsPair);
    }

    public SortedMap<OpponentsPair, ScoresPair> scores() {
        return scores.entrySet().stream()
                .collect(
                        () -> new TreeMap<>(Comparator
                                .comparing((opponents) -> scores.get(opponents).total())
                                .thenComparing(opponentsSortedByStart::indexOf)
                                .reversed()),
                        (map, entry) -> map.put(entry.getKey(), entry.getValue()),
                        TreeMap::putAll
                );
    }
}
