package local;

public record OpponentsPair(String home, String away) {
}
