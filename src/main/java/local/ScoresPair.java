package local;

public record ScoresPair(int home, int away) {
    public ScoresPair {
        if (home < 0 || away < 0)
            throw new IllegalArgumentException("Score cannot be negative");
    }

    public int total() {
        return home + away;
    }
}
