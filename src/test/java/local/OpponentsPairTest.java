package local;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class OpponentsPairTest {
    @Test
    void storesHomeTeamName() {
        var teams = new OpponentsPair("Mexico", "Canada");
        assertEquals("Mexico", teams.home());
    }

    @Test
    void storesAwayTeamName() {
        var teams = new OpponentsPair("Mexico", "Canada");
        assertEquals("Canada", teams.away());
    }

    @Test
    void sameTeamNames_isEqual() {
        assertEquals(
                new OpponentsPair("Mexico", "Canada"),
                new OpponentsPair("Mexico", "Canada")
        );
    }

    @Test
    void differentTeamNames_isNotEqual() {
        assertNotEquals(
                new OpponentsPair("Mexico", "Canada"),
                new OpponentsPair("Spain", "Brazil")
        );
    }
}
