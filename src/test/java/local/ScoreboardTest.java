package local;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ScoreboardTest {
    @Test
    void withNoMatches_returnsEmptyScores() {
        var scoreboard = new Scoreboard();
        var scores = scoreboard.scores();
        assertEquals(Collections.emptySortedMap(), scores);
    }

    @Test
    void upsertingANonExistingMatch_itIsReturnedInScores() {
        var scoreboard = new Scoreboard();
        scoreboard.upsertMatch(
                new OpponentsPair("Mexico", "Canada"),
                new ScoresPair(0, 1)
        );

        var scores = scoreboard.scores();
        assertEquals(
                List.of(Map.entry(
                        new OpponentsPair("Mexico", "Canada"),
                        new ScoresPair(0, 1)
                )),
                scores.sequencedEntrySet().stream().toList()
        );
    }

    @Test
    void upsertingAnExistingMatch_itOverridesItsScore() {
        var scoreboard = new Scoreboard();
        scoreboard.upsertMatch(
                new OpponentsPair("Mexico", "Canada"),
                new ScoresPair(0, 1)
        );
        scoreboard.upsertMatch(
                new OpponentsPair("Mexico", "Canada"),
                new ScoresPair(2, 3)
        );

        var scores = scoreboard.scores();
        assertEquals(
                List.of(Map.entry(
                        new OpponentsPair("Mexico", "Canada"),
                        new ScoresPair(2, 3)
                )),
                scores.sequencedEntrySet().stream().toList()
        );
    }

    @Test
    void upsertingAMatchWithoutPoints_defaultsToZeroToZeroScore() {
        var scoreboard = new Scoreboard();
        scoreboard.upsertMatch(new OpponentsPair("Mexico", "Canada"));

        var scoresEntry = scoreboard.scores().firstEntry();
        assertEquals(
                new ScoresPair(0, 0),
                scoresEntry.getValue()
        );
    }

    @Test
    void removingAMatchFromAnEmptyBoard_itLeavesTheScoresEmpty() {
        var scoreboard = new Scoreboard();
        scoreboard.removeMatch(new OpponentsPair("Mexico", "Canada"));

        var scores = scoreboard.scores();
        assertEquals(Collections.emptySortedMap(), scores);
    }

    @Test
    void removingAMatch_itRemovesItFromTheScores() {
        var scoreboard = new Scoreboard();
        scoreboard.upsertMatch(new OpponentsPair("Mexico", "Canada"));
        scoreboard.upsertMatch(new OpponentsPair("Spain", "Brazil"));
        scoreboard.removeMatch(new OpponentsPair("Mexico", "Canada"));

        var scores = scoreboard.scores();
        assertEquals(
                List.of(Map.entry(
                        new OpponentsPair("Spain", "Brazil"),
                        new ScoresPair(0, 0)
                )),
                scores.sequencedEntrySet().stream().toList()
        );
    }

    @Test
    void withMatchesWithDifferentScores_itReturnsScoresSortedByHighestTotal() {
        var scoreboard = new Scoreboard();
        scoreboard.upsertMatch(
                new OpponentsPair("Mexico", "Canada"),
                new ScoresPair(0, 0));
        scoreboard.upsertMatch(
                new OpponentsPair("Spain", "Brazil"),
                new ScoresPair(1, 1));
        scoreboard.upsertMatch(
                new OpponentsPair("Germany", "France"),
                new ScoresPair(0, 1));

        var scores = scoreboard.scores();
        assertEquals(
                List.of(Map.entry(
                        new OpponentsPair("Spain", "Brazil"),
                        new ScoresPair(1, 1)
                ), Map.entry(
                        new OpponentsPair("Germany", "France"),
                        new ScoresPair(0, 1)
                ), Map.entry(
                        new OpponentsPair("Mexico", "Canada"),
                        new ScoresPair(0, 0)
                )),
                scores.sequencedEntrySet().stream().toList()
        );
    }

    @Test
    void withMatchesWithSameScores_itReturnsScoresSortedByLastInitialInsertion() {
        var scoreboard = new Scoreboard();
        scoreboard.upsertMatch(new OpponentsPair("Mexico", "Canada"));
        scoreboard.upsertMatch(new OpponentsPair("Spain", "Brazil"));

        var scores = scoreboard.scores();
        assertEquals(
                List.of(Map.entry(
                        new OpponentsPair("Spain", "Brazil"),
                        new ScoresPair(0, 0)
                ), Map.entry(
                        new OpponentsPair("Mexico", "Canada"),
                        new ScoresPair(0, 0)
                )),
                scores.sequencedEntrySet().stream().toList()
        );
    }

    @Test
    void updatingExistingMatchToSameScores_itReturnsScoresSortedByLastInitialInsertion() {
        var scoreboard = new Scoreboard();
        scoreboard.upsertMatch(new OpponentsPair("Mexico", "Canada"), new ScoresPair(0, 0));
        scoreboard.upsertMatch(new OpponentsPair("Spain", "Brazil"), new ScoresPair(1, 1));
        scoreboard.upsertMatch(new OpponentsPair("Mexico", "Canada"), new ScoresPair(1, 1));

        var scores = scoreboard.scores();
        assertEquals(
                List.of(Map.entry(
                        new OpponentsPair("Spain", "Brazil"),
                        new ScoresPair(1, 1)
                ), Map.entry(
                        new OpponentsPair("Mexico", "Canada"),
                        new ScoresPair(1, 1)
                )),
                scores.sequencedEntrySet().stream().toList()
        );
    }

    @Test
    void withMultipleMatchesWithSameScore_itReturnsScoresByHighestTotalFirstAndThenByLastInitialInsertion() {
        var scoreboard = new Scoreboard();
        scoreboard.upsertMatch(new OpponentsPair("Mexico", "Canada"),
                new ScoresPair(0, 0));
        scoreboard.upsertMatch(new OpponentsPair("Spain", "Brazil"),
                new ScoresPair(0, 0));
        scoreboard.upsertMatch(new OpponentsPair("Mexico", "Canada"),
                new ScoresPair(1, 0));
        scoreboard.upsertMatch(new OpponentsPair("Germany", "France"),
                new ScoresPair(0, 1));
        scoreboard.upsertMatch(new OpponentsPair("Uruguay", "Italy"),
                new ScoresPair(0, 0));

        var scores = scoreboard.scores();
        assertEquals(
                List.of(Map.entry(
                        new OpponentsPair("Germany", "France"),
                        new ScoresPair(0, 1)
                ), Map.entry(
                        new OpponentsPair("Mexico", "Canada"),
                        new ScoresPair(1, 0)
                ), Map.entry(
                        new OpponentsPair("Uruguay", "Italy"),
                        new ScoresPair(0, 0)
                ), Map.entry(
                        new OpponentsPair("Spain", "Brazil"),
                        new ScoresPair(0, 0)
                )),
                scores.sequencedEntrySet().stream().toList()
        );
    }

    @Test
    void reinsertingAMatchAfterDeletion_itIsReturnedBeforePreviouslyExistingMatches() {
        var scoreboard = new Scoreboard();
        scoreboard.upsertMatch(new OpponentsPair("Mexico", "Canada"));
        scoreboard.upsertMatch(new OpponentsPair("Spain", "Brazil"));
        scoreboard.removeMatch(new OpponentsPair("Mexico", "Canada"));
        scoreboard.upsertMatch(new OpponentsPair("Mexico", "Canada"));

        var scores = scoreboard.scores();
        assertEquals(
                List.of(Map.entry(
                        new OpponentsPair("Mexico", "Canada"),
                        new ScoresPair(0, 0)
                ), Map.entry(
                        new OpponentsPair("Spain", "Brazil"),
                        new ScoresPair(0, 0)
                )),
                scores.sequencedEntrySet().stream().toList()
        );
    }
}
