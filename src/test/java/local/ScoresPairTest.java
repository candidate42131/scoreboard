package local;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ScoresPairTest {
    @Test
    void storesHomeScore() {
        var scores = new ScoresPair(0, 1);
        assertEquals(0, scores.home());
    }

    @Test
    void storesAwayScore() {
        var scores = new ScoresPair(0, 1);
        assertEquals(1, scores.away());
    }

    @Test
    void cannotStoreNegativeHomeScore() {
        var exception = assertThrows(IllegalArgumentException.class, () -> {
            new ScoresPair(-1, 0);
        });
        assertEquals("Score cannot be negative", exception.getMessage());
    }

    @Test
    void cannotStoreNegativeAwayScore() {
        var exception = assertThrows(IllegalArgumentException.class, () -> {
            new ScoresPair(0, -1);
        });
        assertEquals("Score cannot be negative", exception.getMessage());
    }

    @Test
    void returnsTheTotalScore() {
        assertEquals(1, new ScoresPair(0, 1).total());
        assertEquals(3, new ScoresPair(1, 2).total());
    }
}
