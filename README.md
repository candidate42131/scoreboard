# Scoreboard

```java
var mexicoCanada  = new OpponentsPair("Mexico", "Canada");
var spainBrazil   = new OpponentsPair("Spain", "Brazil");
var germanyFrance = new OpponentsPair("Germany", "France");

var scoreboard = new Scoreboard();
scoreboard.upsertMatch(mexicoCanada);
scoreboard.upsertMatch(spainBrazil, new ScoresPair(0, 0));
scoreboard.upsertMatch(mexicoCanada, new ScoresPair(0, 1));
scoreboard.upsertMatch(germanyFrance, new ScoresPair(1, 0));
scoreboard.removeMatch(spainBrazil);

for (var entry : scoreboard.scores().entrySet())
    System.out.println(entry.getKey() + " " + entry.getValue());

// OpponentsPair[home=Germany, away=France] ScoresPair[home=1, away=0]
// OpponentsPair[home=Mexico, away=Canada] ScoresPair[home=0, away=1]
```

## Notes

- **Invalid state is mostly unrepresentable**  
The library was designed so that it is impossible to express an invalid scenario. There is no distinction between starting a match afresh and updating it. A match that was not explicitly started, can still be upserted with mid-game scores.  
This follows the assumption that a downstream system using the library would have not much of a change of recovering from a scenario where a match was not initially created but its score needs to be updated.

- **Unexpressed/Unclear sorting in interface**  
The `Scoreboard#scores` method returns a `SortedMap` of the scores according to the given criteria: highest total scores, followed by most-recently started when the former is equal. However, to a consumer of the API, this might not be obvious without reading some documentation or the implementation. This can be considered a shortcoming of the design of the library. The type `SortedMap`, might give a hint but that is not sufficiently clear.

- **Further separation of concerns**  
Expanding on the limitation of the previous point:  
With a better understanding of the domain and use case, a more feature-rich data holder could be returned instead. Something like a collection of `Match` objects, where perhaps the match start time (and not insertion order), is explicitly returned.
Then, an explicit and more expressive comparator for `Match` can be implemented.

- **Lack of concurrency support**  
The library is not meant to be thread-safe. The collection types used are not thread-safe. Even if thread-safe types were chosen, some additional synchronization effort would be required, because multiple collections are used in tandem (current scores, match starting order) and not updated atomically.

- **Retrieval optimization**  
The library is not optimized for neither insertion nor retrieval. The simplest implementation solution was preferred instead. The order is calculated for each retrieval of the scores.  
However, one can imagine that retrieval speed would be significantly more important. If retrieval was to be optimized, the already-sorted `SortedMap` could be stored instead of being calculated on-demand each time.  
The implemented solution would allow such a refactoring without having to change the interface.

I would be interested in discussing the mentioned and possibly other compromises more extensively.
